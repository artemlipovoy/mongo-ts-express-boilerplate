## Intro
Simple boilerplate for TypeScript-based Express.JS application with JWT-authorization

## Install, Build, Run

Install node package dependencies:

`$ npm install`

Build:

`$ npm run build`

Run in development mode with hot reload:

`$ npm run dev`

## Recommendation

## Enviroment variables

| Env name | Description |
|:-------|:-------|
|PORT| port to listen |
|MONGO_ATLAS| mongodb atlas |
|MONGO_USERNAME| mongodb username |
|MONGO_PASSWORD| mongodb password |
|MONGO_HOSTNAME| mongodb hostname |
|MONGO_PORT| mongodb port |
|MONGO_DB| mongodb db name |
|JWTSALT| salt for jwt tokens |
