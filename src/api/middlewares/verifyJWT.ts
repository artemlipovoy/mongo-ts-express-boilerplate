import {Request, Response,} from "express";
import Auth from "../../services/Auth";

export const verifyJWT = async (req: Request, res: Response, next) => {
    try {
        if(req.headers.authorization){
            const [type, token] = req.headers.authorization.split(' ')
            if(type==="Bearer") {
                req.user = await Auth.verifyToken(token)
                next()
            }else{
                throw {
                    error: "Invalid authorization type",
                    code: 401
                }
            }
        }
        else{
            throw {
                error: "Missing authorization header",
                code: 401
            }
        }
    }catch(e){
        return res.status(e.code).json(e)
    }
}