import {Request, Response, Router} from "express";
import Auth from "../../services/Auth";
import {check} from "express-validator";
import {returnValidationResult} from "../middlewares/returnValidationResult";
import {verifyJWT} from "../middlewares/verifyJWT";

export const auth = (router: Router) => {
    router.post(
        '/auth/signup',
        [
            check('email', 'Invalid email').isEmail(),
            check('password', 'Minimum password length 6 characters').isLength({min: 6})
        ],
        returnValidationResult,
        async (req: Request, res: Response) => {
            try {
                const {email, password} = req.body
                const signUpResult = await Auth.signUp({email, password})
                return res.status(200).json(signUpResult)
            }catch(e){
                return res.status(e.code).json(e)
            }
        })
    router.post(
        '/auth/signin',
        [
            check('email', 'Invalid email').isEmail(),
            check('password', 'Minimum password length 6 characters').isLength({min: 6})
        ],
        returnValidationResult,
        async (req: Request, res: Response) => {
            try {
                const {email, password} = req.body
                const signInResult = await Auth.signIn({email, password})
                return res.status(200).json(signInResult)
            }catch(e){
                return res.status(e.code).json(e)
            }
        })
}
