import {Router} from "express";
import {auth} from "./routes/auth";

export const routes = ():Router => {
    const app: Router = Router()
    auth(app)
    return app
}