require('dotenv').config()

export const config = {
    port: process.env.PORT?process.env.PORT:8080,
    apiPrefix: '',
    mongoUrl: process.env.MONGO_ATLAS?process.env.MONGO_ATLAS:`mongodb://${process.env.MONGO_USERNAME}:${process.env.MONGO_PASSWORD}@${process.env.MONGO_HOSTNAME}:${process.env.MONGO_PORT}/${process.env.MONGO_DB}?authSource=admin`,
    jwtSalt: process.env.JWTSALT
}