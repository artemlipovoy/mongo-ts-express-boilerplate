import mongoose, {Connection} from "mongoose"
import {config} from "../config";

export const db = async ():Promise<Connection> => {
    try{
        const connection = await mongoose.connect(config.mongoUrl, {
            useNewUrlParser: true,
            useUnifiedTopology: true
        })
        return connection.connection
    } catch(e){
        console.log(e)
    }
}