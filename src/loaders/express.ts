import * as bodyParser from "body-parser";
import cors from "cors";
import {config} from "../config";
import {routes} from "../api"
import {Application, Request, Response} from "express";

export const express = (app: Application):void => {
    app.get('/status', (req: Request, res: Response) => {
        return res.status(200).end()
    })
    app.use(cors())
    app.use(bodyParser.json())
    app.use(config.apiPrefix, routes())
}