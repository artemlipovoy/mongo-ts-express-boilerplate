import {express} from "./express";
import {Application} from "express";
import {db} from "./db";

export const loaders = async (app: Application):Promise<boolean> => {
    try{
        await db()
        express(app)
        return true
    }catch (e) {
        console.log(e)
    }
}