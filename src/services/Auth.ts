import {IUser, IUserDTO} from "../interfaces/IUser";
import {User} from "./schemas/User";
import {createHash, randomBytes} from "crypto";
import {config} from "../config";
import jwt from "jsonwebtoken";

export default class Auth{

    private static async isExists(email: string): Promise<boolean>{
        const user = await User.findOne({email})
        return !!user;
    }

    public static async verifyToken(token: string): Promise<IUser> {
        try {
            const verificationResult = await jwt.verify(token, config.jwtSalt)
            return await User.findById(verificationResult.id)
        } catch (e) {
            if(e.name === 'JsonWebTokenError'){
                throw {
                    error: "Invalid JWT Token",
                    code: 401
                }
            }
            throw {
                error: e.error||'Internal server error',
                code: e.code||500
            }
        }
    }

    public static async signUp(userDTO: IUserDTO): Promise<{id:string, jwt: string}> {
        try{
            const checkEmailUnique = await this.isExists(userDTO.email)
            if(checkEmailUnique){
                throw {
                    error: `Email ${userDTO.email} is already in use`,
                    code: 400
                }
            }
            const salt = randomBytes(32).toString('hex')
            const hash = createHash('sha256')

            const newUser = new User({
                ...userDTO,
                salt: salt,
                password: hash.update(salt+userDTO.password+salt).digest('hex')
            })

            const user = await newUser.save()
            return {
                id: user._id,
                jwt: jwt.sign({id: user._id}, config.jwtSalt)
            }
        }catch (e) {
            throw {
                error: e.error||'Internal server error',
                code: e.code||500
            }
        }
    }

    public static async signIn(userDTO: IUserDTO): Promise<{id: string, jwt: string}> {
        try{
            const user = await User.findOne({email: userDTO.email})
            if(!user){
                throw {
                    error: 'Incorrect login data',
                    code: 400
                }
            }
            const passHash = createHash('sha256')
                .update(user.salt+userDTO.password+user.salt)
                .digest('hex')
            if(passHash !== user.password){
                throw {
                    error: 'Incorrect login data',
                    code: 400
                }
            }
            return {
                id: user._id,
                jwt: jwt.sign({id: user._id}, config.jwtSalt)
            }
        }catch(e){
            throw {
                error: e.error||'Internal server error',
                code: e.code||500
            }
        }
    }
}