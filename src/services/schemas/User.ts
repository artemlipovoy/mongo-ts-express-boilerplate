import {Schema, model} from "mongoose";
import {IUser} from "../../interfaces/IUser";

const schema = new Schema({
    email: String,
    password: String,
    salt: String
})

export const User = model<IUser>("User", schema)