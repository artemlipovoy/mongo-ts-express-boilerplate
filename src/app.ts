import express, {Application} from "express";
import {config} from "./config";
import {loaders} from "./loaders";

async function startServer():Promise<Application>{
    try{
        const app: Application = express()
        await loaders(app)
        console.log('Server initialized')
        app.listen(config.port, ()=>
            console.log('Server started')
        )
        return app
    }catch (e) {
        console.log(e)
    }
}

const server = startServer()