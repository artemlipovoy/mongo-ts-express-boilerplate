import {IUser} from "../interfaces/IUser";

declare module "express"{
    interface Request {
        user: IUser
    }
}