FROM node:14
WORKDIR /home/app
COPY package*.json ./
RUN npm install
COPY src .
COPY tsconfig.json ./
RUN npm run build

CMD ["node","dist/app.js"]
